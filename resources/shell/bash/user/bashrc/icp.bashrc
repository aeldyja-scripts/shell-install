#!/bin/bash

#LS Colors

export LS_COLORS=${LS_COLORS/di=01;34/di=01;30} # LS colors for dark/blue background

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -d ~/.icp/bash/aliases ]; then
    source ~/.icp/bash/aliases/*
fi

#export EDITOR="vim"
#export VISUAL="vim"

#. "${HOME}/.vim_aliases"
